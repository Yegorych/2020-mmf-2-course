def circular_shift(arr1, arr2, steps):
    
    if (steps > len(arr2)):
        steps = steps % len(arr2)
    
    if (steps < 0):
        arr2 = arr2[abs(steps):] + arr2[:abs(steps)]
    else:
        steps = abs(steps)
        arr2 = arr2[len(arr2) - steps:] + arr2[:len(arr2) - steps]

    # print(arr2)
    
    for i in range(0, len(arr2)):
        if (arr2[i] != arr1[i]):
            return False

    return True
