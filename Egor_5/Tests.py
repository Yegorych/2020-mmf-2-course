import Test
from solve import circular_shift

Test.assert_equals(circular_shift(
[1, 2, 3, 4],
[3, 4, 1, 2],
2
), True)
Test.assert_equals(circular_shift(
[1, 1],
[1, 1],
6
), True)
Test.assert_equals(circular_shift(
[0, 1, 2, 3, 4, 5],
[3, 4, 5, 2, 1, 0],
3
), False)
Test.assert_equals(circular_shift(
[0, 1, 2, 3],
[1, 2, 3, 1],
1
), False)
Test.assert_equals(circular_shift(
list(range(32)),
list(range(32)),
0
), True)
Test.assert_equals(circular_shift(
[1, 2, 1],
[1, 2, 1],
3
), True)
Test.assert_equals(circular_shift(
[5, 7, 2, 3],
[2, 3, 5, 7],
-2
), True)