import test
from solve import shifrCezarya

test.assert_equals(shifrCezarya("X adkt eniwdc", 11), "I love python")
test.assert_equals(shifrCezarya("Jqts Rfxp", 21), "Elon Mask")
test.assert_equals(shifrCezarya("R anjm cqn unccna. Bcxxm dy. Bjc mxfw. Yxwmnanm oxa j vrwdcn. Cqnw ananjm cqn unccna jpjrw", 17), "I read the letter. Stood up. Sat down. Pondered for a minute. Then reread the letter again")



