def shifrCezarya(message, shift):
    newMessage = ""
    for symbol in message:
        if symbol >= "a" and symbol <= "z":
            difference = ord(symbol) - ord("a")
            difference = (difference + shift) % 26
            newSymbol = chr(difference + ord("a"))
            newMessage += newSymbol
        elif "A" <= symbol and symbol <= "Z":
            difference = ord(symbol) - ord("A")
            difference = (difference + shift) % 26
            newSymbol = chr(difference + ord("A"))
            newMessage += newSymbol
        else:
            newMessage += symbol
    
    return newMessage

print(shifrCezarya("I read the letter. Stood up. Sat down. Pondered for a minute. Then reread the letter again", 9))

