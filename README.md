# Заключительное задание

## Групповое задание :

  * ### Визуализация группового задания [WebPage](https://bitbucket.org/Yegorych/2020-mmf-2-course/src/master/results%20visualization/)
  * ### Групповое задание по созданию веб-страницы [WebPage](https://bitbucket.org/Yegorych/2020-mmf-2-course/src/master/webPage/) и ссылка на [сайт](https://bookingbyegor.herokuapp.com/)
 
## Индивидуальные задания :

  * ### Индивидуальное задание №1 [вариант 6](https://bitbucket.org/Yegorych/2020-mmf-2-course/src/master/Egor_5/)
  * ### Индивидуальное задание №2 [вариант 6](https://bitbucket.org/Yegorych/2020-mmf-2-course/src/master/Egor_6/)
  * ### Индивидуальное задание №3 [вариант 2](https://bitbucket.org/Yegorych/2020-mmf-2-course/src/master/Egor_7/)


#### Предыдущие [задания](https://bitbucket.org/Yegorych/2020-mmf-2-course/src/master/Previous%20task/)




